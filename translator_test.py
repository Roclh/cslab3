#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=invalid-name
# pylint: disable=line-too-long
import unittest

from translator import parse, allocate, preprocess, tokenize
from isa import Addressing


class TestTranslatatorHello(unittest.TestCase):

    def test_preprocess(self):
        raw = '''section data:
    hello: 'Hello, World!',0
    hello_ptr: hello

section text:
    _start:
    write:
        ld $hello_ptr
        cmp #0
        jeq end
        output
        ld hello_ptr
        add #1
        st hello_ptr
        jmp write
    end:
        halt'''
        post_processed = preprocess(raw)

        self.assertEqual(post_processed, "section data: hello: 'Hello, World!',0 hello_ptr: hello "
                         "section text: _start: write: ld $hello_ptr cmp #0 jeq end output ld hello_ptr add #1 st hello_ptr jmp write end: halt")

    def test_tokenize(self):
        data_tokens, code_tokens = tokenize(
            "section data: hello: 'Hello, World!',0 hello_ptr: hello "
            "section text: _start: write: ld $hello_ptr cmp #0 jeq end output ld hello_ptr add #1 st hello_ptr jmp write end: halt"
        )

        self.assertEqual(data_tokens, [('hello',), '72', '101', '108', '108', '111', '44',
                         '32', '87', '111', '114', '108', '100', '33', '0', ('hello_ptr',), 'hello'])
        self.assertEqual(code_tokens, [('_start',), ('write',), 'ld', '$hello_ptr', 'cmp', '#0', 'jeq',
                                       'end', 'output', 'ld', 'hello_ptr', 'add', '#1', 'st', 'hello_ptr', 'jmp', 'write', ('end',), 'halt'])

    def test_allocate(self):
        tokens = [('hello',), '72', '101', '108', '108', '111', '44',
                  '32', '87', '111', '114', '108', '100', '33', '0', ('hello_ptr',), 'hello']
        data, labels = allocate(tokens)
        self.assertEqual(data, ['72', '101', '108', '108', '111', '44',
                         '32', '87', '111', '114', '108', '100', '33', '0', 0])
        self.assertDictEqual(
            labels, {'hello': 0, 'hello_ptr': 14})

    def test_parse(self):
        tokens = [('_start',), ('write',), 'ld', '$hello_ptr', 'cmp', '#0', 'jeq',
                  'end', 'output', 'ld', 'hello_ptr', 'add', '#1', 'st', 'hello_ptr', 'jmp', 'write', ('end',), 'halt']
        code, labels = parse(tokens)

        benchmark = [
            {'opcode': 'LD', 'arg': 'hello_ptr',
                'addressing': Addressing.INDIRECT},
            {'opcode': 'CMP', 'arg': '0', 'addressing': Addressing.IMMEDIATE},
            {'opcode': 'JEQ', 'arg': 'end', 'addressing': Addressing.DIRECT},
            {'opcode': 'OUTPUT', 'arg': '0', 'addressing': Addressing.DIRECT},
            {'opcode': 'LD', 'arg': 'hello_ptr', 'addressing': Addressing.DIRECT},
            {'opcode': 'ADD', 'arg': '1', 'addressing': Addressing.IMMEDIATE},
            {'opcode': 'ST', 'arg': 'hello_ptr', 'addressing': Addressing.DIRECT},
            {'opcode': 'JMP', 'arg': 'write', 'addressing': Addressing.DIRECT},
            {'opcode': 'HALT', 'arg': '0', 'addressing': Addressing.DIRECT}
        ]
        self.assertDictEqual(labels, {'_start': 0, 'write': 0, 'end': 8})
        for instr_idx, instr in enumerate(benchmark):
            self.assertEqual(code[instr_idx]["opcode"], instr["opcode"])
            self.assertEqual(str(code[instr_idx]["arg"]), instr["arg"])
            self.assertEqual(
                code[instr_idx]["addressing"], instr["addressing"])


class TestTranslatatorCat(unittest.TestCase):

    def test_preprocess(self):
        raw = '''section data:
  section text:
      _start:

      read:
          input
          cmp #0
          jeq end
          output
          jmp read
      end:
          halt'''
        post_processed = preprocess(raw)

        self.assertEqual(
            post_processed, 'section data: section text: _start: read: input cmp #0 jeq end output jmp read end: halt')

    def test_tokenize(self):
        data_tokens, code_tokens = tokenize(
            "section data: section text: _start: read: input cmp #0 jeq end output jmp read end: halt")

        self.assertListEqual(data_tokens, [])
        self.assertListEqual(code_tokens, [('_start',), ('read',), 'input',
                             'cmp', '#0', 'jeq', 'end', 'output', 'jmp', 'read', ('end',), 'halt'])

    def test_allocate(self):
        tokens = []
        data, labels = allocate(tokens)
        self.assertListEqual(data, [])
        self.assertDictEqual(
            labels, {})

    def test_parse(self):
        tokens = [('_start',), ('read',), 'input',
                  'cmp', '#0', 'jeq', 'end', 'output', 'jmp', 'read', ('end',), 'halt']
        code, labels = parse(tokens)

        benchmark = [
            {'opcode': 'INPUT', 'arg': '0', 'addressing': Addressing.DIRECT},
            {'opcode': 'CMP', 'arg': '0', 'addressing': Addressing.IMMEDIATE},
            {'opcode': 'JEQ', 'arg': 'end', 'addressing': Addressing.DIRECT},
            {'opcode': 'OUTPUT', 'arg': '0', 'addressing': Addressing.DIRECT},
            {'opcode': 'JMP', 'arg': 'read', 'addressing': Addressing.DIRECT},
            {'opcode': 'HALT', 'arg': '0', 'addressing': Addressing.DIRECT}
        ]
        self.assertDictEqual(
            labels, {'_start': 0, 'end': 5, 'read': 0})
        for instr_idx, instr in enumerate(benchmark):
            self.assertEqual(code[instr_idx]["opcode"], instr["opcode"])
            self.assertEqual(str(code[instr_idx]["arg"]), instr["arg"])
            self.assertEqual(
                code[instr_idx]["addressing"], instr["addressing"])
