# pylint: disable=invalid-name
# pylint: disable=missing-function-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring

import json
from collections import deque
from enum import Enum

OPCODE_SIZE = 5
ADDR_SIZE = 2
ARG_SIZE = 25

arg_m = 0b1111111111111111111111111_00_00000
addr_m = 0b0000000000000000000000000_11_00000
op_m = 0b0000000000000000000000000_00_11111

op_offs = 0
addr_offs = OPCODE_SIZE
arg_offs = OPCODE_SIZE + ADDR_SIZE


class Addressing(int, Enum):
    IMMEDIATE = 0
    DIRECT = 1
    INDIRECT = 2


class Instruction:
    @staticmethod
    def encode(opcode: int, arg: int, addressing: int) -> int:
        instr = 0
        instr += (opcode & op_m)
        instr += (addressing << addr_offs) & addr_m
        instr += (arg << arg_offs) & arg_m
        return instr

    @staticmethod
    def fetch_opcode(instr: int) -> int:
        return instr & op_m

    @staticmethod
    def fetch_addressing(instr: int) -> int:
        return (instr & addr_m) >> addr_offs

    @staticmethod
    def fetch_arg(instr: int) -> int:
        return (instr & arg_m) >> arg_offs


class Opcode(int, Enum):
    HALT = 0
    LD = 1
    ST = 2
    CMP = 3
    ADD = 4
    SUB = 5
    MUL = 6
    DIV = 7
    MOD = 8
    WFI = 9     # wfi - wait for interruption
    OUTPUT = 10
    JMP = 11
    JEQ = 12
    JNE = 13
    JLT = 14
    JGT = 15
    JLE = 16
    JGE = 17
    EOI = 18    # eoi - end of interruption


opcodes_by_number = dict((opcode.value, opcode) for opcode in Opcode)
opcodes_by_name = dict((opcode.name, opcode) for opcode in Opcode)
addressing_by_name = dict((addressing.name, addressing) for addressing in Addressing)


def encode_instr(instr):
    bin_instr = bytearray()
    opcode = instr["opcode"]
    arg = instr["arg"]
    addressing = instr["addressing"]
    coded = Instruction.encode(int(opcode.value), int(arg), addressing)
    for _ in range(4):
        bin_instr.append(coded & 255)
        coded = coded >> 8
    return bytes(bin_instr)


def format_instr(instr):
    opcode = Instruction.fetch_opcode(instr)
    if opcodes_by_number[opcode].name in ["HALT", "OUTPUT", "EOI"]:
        return f"{opcodes_by_number[opcode].name}"
    addressing = Instruction.fetch_addressing(instr)
    arg = Instruction.fetch_arg(instr)
    return f"{opcodes_by_number[opcode].name} {['#', '', '$'][addressing]}{arg}"


def write_json_code(filename: str, data: list, code: list):
    """Записать машинный код в json файл."""
    structed_code = deque()
    for instr in code:
        struct_instr = instr.copy()
        struct_instr["addressing"] = instr['addressing'].name
        structed_code.append(struct_instr)
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(
            {"data": data, "code": list(structed_code)}, indent=4))


def read_json_code(filename: str):
    memory = []
    with open(filename, encoding="utf-8") as file:
        program = json.loads(file.read())
    for data in program["data"]:
        memory.append(data)
    _start_ = len(memory)
    for instr in program["code"]:
        instr["opcode"] = opcodes_by_name[instr["opcode"]]
        instr["arg"] = int(instr["arg"])
        instr["addressing"] = addressing_by_name[instr["addressing"]]
        memory.append(int.from_bytes(encode_instr(instr), "little"))
    return memory[0:], _start_
