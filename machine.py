#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=invalid-name
# pylint: disable=too-many-instance-attributes
# pylint: disable=broad-except
# pylint: disable=too-many-branches

import logging
import sys
from collections import deque

from isa import Addressing, Opcode, read_json_code, format_instr, Instruction


class DataPath:
    memory: list[int]
    z_flag = 0  # Zero flag
    l_flag = 0  # Less flag

    def __init__(self, program: list[int], pc: int):
        self.memory = program  # Memory with program and data
        self.ip = pc  # Next instruction address
        self.acc = 0  # Accumulator
        self.ir = 0
        self.ar = 0  # Address register
        self.a, self.b = 0, 0  # ALU args
        self.computed = 0  # ALU result
        self.output_buffer = deque()

    def select_instruction(self) -> int:
        self.latch_ar_from_ip()
        self.ir = self.memory[self.ar]
        self.ip += 1
        return Instruction.fetch_opcode(self.ir)

    def addressing_type(self):
        return Addressing(Instruction.fetch_addressing(self.ir))

    def latch_a_alu(self):
        self.a = self.acc

    def latch_b_alu_from_arg(self):
        self.b = Instruction.fetch_arg(self.ir)

    def latch_b_alu_from_mem(self):
        self.b = self.memory[self.ar]

    def calculate(self, opcode: Opcode):
        if isinstance(self.a, str):
            self.a = int(self.a)
        if isinstance(self.b, str):
            self.b = int(self.b)
        if opcode is Opcode.ADD:
            self.computed = self.a + self.b
        elif opcode in (Opcode.SUB, Opcode.CMP):
            self.computed = self.a - self.b
        elif opcode is Opcode.MUL:
            self.computed = self.a * self.b
        elif opcode is Opcode.DIV:
            self.computed = self.a // self.b
        elif opcode is Opcode.MOD:
            self.computed = self.a % self.b
        self.computed = int(self.computed)
        self.z_flag = self.computed == 0
        self.l_flag = self.computed < 0

    def latch_acc_from_alu(self):
        self.acc = self.computed

    def latch_acc_from_mem(self):
        self.acc = self.memory[self.ar]

    def latch_acc_from_arg(self):
        self.acc = Instruction.fetch_arg(self.ir)

    def latch_acc_from_interruption_controller(self, inp: str):
        self.acc = int(inp)

    def output(self):
        if isinstance(self.acc, str):
            self.acc = int(self.acc)
        self.output_buffer.append(chr(self.acc))

    def store_to_mem(self):
        self.memory[self.ar] = self.acc

    def latch_ip(self):
        self.ip = Instruction.fetch_arg(self.ir)

    def latch_ar_from_arg(self):
        self.ar = Instruction.fetch_arg(self.ir)

    def latch_ar_from_ip(self):
        self.ar = self.ip

    def latch_ar_from_mem(self):
        self.ar = self.memory[self.ar]


class ControlUnit:
    current_tick = 0
    data_path: DataPath

    def __init__(self, program, pc: int, inter_input: list[{int, str}]):
        self.interruptionController = InterruptionController(inter_input)
        self.data_path = DataPath(program, pc)

    def tick(self):
        logging.debug("%s", self)
        self.current_tick += 1
        self.interruptionController.tick(self.current_tick)

    def get_tick(self):
        return self.tick

    def is_interruption_required(self):
        return self.interruptionController.require_interruption

    def decode_instruction(self):
        opcode = Opcode(self.data_path.select_instruction())
        self.tick()
        if opcode is Opcode.HALT:
            raise StopIteration

        if opcode is Opcode.JMP:
            self.data_path.latch_ip()

        if opcode is Opcode.WFI:
            if self.interruptionController.input_exists():
                self.data_path.latch_ip()
                self.interruptionController.set_interuption_ar(self.data_path.ip + 1)

        if opcode is Opcode.EOI:
            if self.interruptionController.in_interruption:
                self.interruptionController.in_interruption = False
                self.interruptionController.load_state(self.data_path)

        if opcode is Opcode.OUTPUT:
            self.data_path.output()

        if any([
            opcode is Opcode.JMP,
            opcode is Opcode.JEQ and self.data_path.z_flag,
            opcode is Opcode.JNE and not self.data_path.z_flag,
            opcode is Opcode.JLT and self.data_path.l_flag,
            opcode is Opcode.JLE and (self.data_path.l_flag or self.data_path.z_flag),
            opcode is Opcode.JGT and not self.data_path.l_flag and not self.data_path.z_flag,
            opcode is Opcode.JGE and not self.data_path.l_flag
        ]):
            self.data_path.latch_ip()

        if opcode is Opcode.LD:
            if self.data_path.addressing_type() is Addressing.IMMEDIATE:
                self.data_path.latch_acc_from_arg()
            elif self.data_path.addressing_type() is Addressing.DIRECT:
                self.data_path.latch_ar_from_arg()
                self.tick()
                self.data_path.latch_acc_from_mem()
            elif self.data_path.addressing_type() is Addressing.INDIRECT:
                self.data_path.latch_ar_from_arg()
                self.tick()
                self.data_path.latch_ar_from_mem()
                self.tick()
                self.data_path.latch_acc_from_mem()
        if opcode is Opcode.ST:
            self.data_path.latch_ar_from_arg()
            if self.data_path.addressing_type() is Addressing.INDIRECT:
                self.tick()
                self.data_path.latch_ar_from_mem()
            self.data_path.store_to_mem()

        if opcode in [Opcode.ADD, Opcode.SUB, Opcode.MOD,
                      Opcode.MUL, Opcode.DIV, Opcode.CMP]:
            self.data_path.latch_a_alu()
            if self.data_path.addressing_type() is Addressing.IMMEDIATE:
                self.data_path.latch_b_alu_from_arg()
            elif self.data_path.addressing_type() is Addressing.DIRECT:
                self.data_path.latch_ar_from_arg()
                self.tick()
                self.data_path.latch_b_alu_from_mem()
            elif self.data_path.addressing_type() is Addressing.INDIRECT:
                self.data_path.latch_ar_from_arg()
                self.tick()
                self.data_path.latch_ar_from_mem()
                self.tick()
                self.data_path.latch_b_alu_from_mem()
            self.data_path.calculate(opcode)

            if opcode is not Opcode.CMP:
                self.data_path.latch_acc_from_alu()
        self.tick()
        self.interruptionController.check_is_interruption_required()
        if self.interruptionController.require_interruption and not self.interruptionController.in_interruption:
            self.interruptionController.save_state(self.data_path)
            self.interruptionController.in_interruption = True
            self.data_path.ip = self.interruptionController.interuption_ar
            self.data_path.acc = self.interruptionController.get_input()
            self.tick()

    def __repr__(self):
        state = f"{{TICK: {self.current_tick}" \
                f" ACC: {self.data_path.acc}" \
                f" IP: {self.data_path.ip}" \
                f" IR: {self.data_path.ir}" \
                f" AR: {self.data_path.ar}" \
                f" INTER: {self.interruptionController.in_interruption}" \
                f" INTER_AR: {self.interruptionController.interuption_ar}}}"

        alu = f"ALU [a:{self.data_path.a} b:{self.data_path.b} => {self.data_path.computed}]" \
              f" FLAGS: [Z:{int(self.data_path.z_flag)} L:{int(self.data_path.l_flag)}]"
        action = f" Action: {format_instr(self.data_path.ir)}"

        return f"{state} {alu} {action}"


class InterruptionController:
    queue = []
    inter_input = []
    acc = 0
    ip = 0
    z_flag = 0
    l_flag = 0
    a, b = 0, 0
    computed = 0
    ar = 0
    ir = 0
    require_interruption = False
    in_interruption = False
    interuption_ar = 0

    def __init__(self, inter_input: list[{int, str}]):
        self.inter_input = inter_input

    def tick(self, tick: int):
        for inp in self.inter_input:
            if int(inp[0]) == tick:
                self.queue.append(inp)
                self.inter_input.remove(inp)
                self.require_interruption = True
                return

    def save_state(self, dataPath: DataPath):
        self.acc = dataPath.acc
        self.ip = dataPath.ip
        self.z_flag = dataPath.z_flag
        self.l_flag = dataPath.l_flag
        self.a, self.b = dataPath.a, dataPath.b
        self.computed = dataPath.computed
        self.ar = dataPath.ar
        self.ir = dataPath.ir
        self.in_interruption = True

    def load_state(self, dataPath: DataPath):
        dataPath.acc = self.acc
        dataPath.ip = self.ip
        dataPath.z_flag = self.z_flag
        dataPath.l_flag = self.l_flag
        self.require_interruption = len(self.queue) == 0

    def check_is_interruption_required(self):
        if len(self.queue) == 0:
            self.require_interruption = False
        else:
            self.require_interruption = True

    def get_input(self):
        if self.require_interruption:
            return ord(self.queue.pop(0)[1])

    def input_exists(self):
        return len(self.inter_input) != 0

    def is_in_interruption(self):
        return self.in_interruption

    def set_interuption_ar(self, ar: int):
        self.interuption_ar = ar


def show_memory(memory):
    memory_state = ""
    for address, cell in enumerate(reversed(memory)):
        address = len(memory) - address - 1
        address_br = bin(address)[2:]
        address_br = (10 - len(address_br)) * "0" + address_br
        cell = int(cell)
        cell_br = bin(cell)[2:]
        cell_br = (32 - len(cell_br)) * "0" + cell_br
        memory_state += f"({address:5})\
            [{address_br:10}]  -> [{cell_br:32}] -> ({cell:10})"
        try:
            memory_state += f" ~ {format_instr(cell):20}"
        except Exception:
            pass
        finally:
            memory_state += "\n"

    return memory_state


def simulation(program: list[int], pc: int, input_tokens, limit):
    logging.info("{ INPUT MESSAGE } [ `%s` ]", "".join(list(map(lambda token: token[1], input_tokens))))
    logging.info("{ INPUT TOKENS  }  %s ", str(input_tokens))

    control_unit = ControlUnit(program, pc, input_tokens)
    instruction_counter = 0
    try:
        while True:
            if not limit > instruction_counter:
                print("too long execution, increase limit!")
                break
            control_unit.decode_instruction()
            instruction_counter += 1

    except EOFError:
        logging.warning('Input buffer is empty!')
    except StopIteration:
        pass
    finally:
        logging.debug("%s", f"Memory map is\n{show_memory(program)}")

    return ''.join(control_unit.data_path.output_buffer), instruction_counter, control_unit.current_tick


def main(args):
    if len(args) != 2 and len(args) != 1:
        logging.error("Wrong arguments: machine.py <code.bin> [input]")
        return

    if len(args) == 2:
        code_file, input_file = args

        program, start = read_json_code(code_file)

        with open(input_file, encoding="utf-8") as file:
            input_tokens = file.read().split("\n")

        input_tokens = list(map(lambda token: token.split(","), input_tokens))

        output, instr_counter, ticks = simulation(
            program, start,
            input_tokens=input_tokens,
            limit=1000
        )

        print(f"Output is `{''.join(output)}`")
        print(f"instr_counter: {instr_counter} ticks: {ticks}")
        return
    if len(args) == 1:
        code_file = args[0]

        program, start = read_json_code(code_file)

        output, instr_counter, ticks = simulation(
            program, start,
            input_tokens=[],
            limit=1000
        )

        print(f"Output is `{''.join(output)}`")
        print(f"instr_counter: {instr_counter} ticks: {ticks}")
        return


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    main(sys.argv[1:])
    #main([".\programs\hello.json"])
