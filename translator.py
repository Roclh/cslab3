#!/usr/bin/python3
# pylint: disable=missing-function-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-module-docstring
import json
import re
import sys
from collections import deque

from isa import Addressing, write_json_code


def preprocess(raw: str) -> str:
    lines = []
    for line in raw.split("\n"):
        # remove comments
        comment_idx = line.find(";")
        if comment_idx != -1:
            line = line[:comment_idx]
        # remove leading spaces
        line = line.strip()

        lines.append(line)

    text = " ".join(lines)
    text = re.sub(" +", " ", text)
    return text


def parse(tokens):
    labels = {}
    code = deque()
    has_arg = False
    for token in tokens:
        if isinstance(token, tuple):
            labels[token[0]] = len(code)
        else:
            token_upper = token.upper()
            if has_arg:
                addressing = Addressing.DIRECT
                if token[0] == '#':
                    addressing = Addressing.IMMEDIATE
                    token = token[1:]
                if token[0] == '$':
                    addressing = Addressing.INDIRECT
                    token = token[1:]

                code[-1]["arg"] = token
                code[-1]["addressing"] = addressing
                has_arg = False
            else:
                code.append({"opcode": token_upper, "arg": '0', "addressing": Addressing.DIRECT})
                if token_upper in ["HALT", "INPUT", "OUTPUT", "EOI"]:
                    has_arg = False
                else:
                    has_arg = True
    return list(code), labels


def allocate(tokens):
    data = []
    labels = {}
    for token in tokens:
        if isinstance(token, tuple):
            labels[token[0]] = len(data)
        elif token.isdigit():
            data.append(token)
        else:
            data.append(labels[token])
    return data, labels


def tokenize(text):
    text = re.sub(
        r"'.*'",
        lambda match: f'{",".join(map(lambda char: str(ord(char)), match.group()[1:-1]))}',
        text
    )
    data_section_index = text.find("section data:")
    text_section_index = text.find("section text:")

    data_tokens = re.split(
        "[, ]", text[data_section_index + len("section data:"): text_section_index])
    data_tokens = list(filter(lambda token: token, data_tokens))
    data_tokens = list(
        map(lambda token: (token[:-1],) if token[-1] == ':' else token, data_tokens))
    text_tokens = re.split(
        "[, ]", text[text_section_index + len("section text:"):])
    text_tokens = list(filter(lambda token: token, text_tokens))
    text_tokens = list(
        map(lambda token: (token[:-1],) if token[-1] == ':' else token, text_tokens))
    return data_tokens, text_tokens


def translate(source: str):
    preprocessed_code = preprocess(source)
    data_tokens, code_tokens = tokenize(preprocessed_code)
    data, data_labels = allocate(data_tokens)
    code, code_labels = parse(code_tokens)
    labels = data_labels.copy()
    for key, value in code_labels.items():
        labels[key] = value + len(data)
    for word_idx, word in enumerate(code):
        if isinstance(word, dict):
            if word["arg"] in labels:
                code[word_idx]["arg"] = labels[word["arg"]]
    return data, code


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: translator.py <input_file> <target_json_file>"

    source, target_json = args

    with open(source, "rt", encoding="utf-8") as file:
        source = file.read()

    data, code = translate(source)

    write_json_code(target_json, data, code)

    print(
        f"source LoC: {len(source.split())} code instr: {len(code)} code bytes: {len(json.dumps(code).encode('utf-8'))}")


if __name__ == '__main__':
    main(sys.argv[1:])
