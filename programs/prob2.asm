section data:
    cap: 4000000
    prev: 0
    temp: 0
    current: 1
    total: 0
section text:
    loop:
        ld current ; Загрузить последнее число последовательности
        add prev ; Прибавить к нему предыдущее
        st temp ; Временно сохранить значение
        ld current ; Загрузить последнее число последовательности до добавления
        st prev ; Сохранить в предыдущее
        ld temp ; Загрузить временное значение
        st current ; Сохранить в текущее
        cmp cap ; проверить не дошло ли значение до лимита
        jge end ; перейти в конец если дошло
        mod #2 ; проверка на четность
        cmp #0
        jeq odd ; переход если чётное
        jmp loop ; цикл
    odd:
        ld total ; загрузить текущее
        add current ; прибавить к нему сумму
        st total ; сохранить в сумму
        jmp loop ; цикл
    end:
        halt

