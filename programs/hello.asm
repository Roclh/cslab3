section data:
    hello: 'Hello, World!',0
    hello_ptr: hello

section text:
    _start:
    write:
        ld $hello_ptr
        cmp #0
        jeq end
        output
        ld hello_ptr
        add #1
        st hello_ptr
        jmp write
    end:
        halt
