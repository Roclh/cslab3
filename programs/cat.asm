section data:
section text:
    _start:

    read:
        wfi read
        cmp #0
        jeq end
        output
        eoi
        jmp read
    end:
        halt
